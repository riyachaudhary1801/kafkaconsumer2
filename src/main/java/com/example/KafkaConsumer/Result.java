package com.example.KafkaConsumer;

import java.io.Serializable;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Result implements Serializable{
	  @Id
	  int id;
	  
    public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	private String name;
    private String percentage;
    private String result;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPercentage() {
		return percentage;
	}
	public void setPercentage(String percentage) {
		this.percentage = percentage;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	
	
    
}
