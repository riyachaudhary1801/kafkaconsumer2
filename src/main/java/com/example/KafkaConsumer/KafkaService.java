package com.example.KafkaConsumer;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Service;

@Service
public class KafkaService {

	@Autowired
	private resultInterface repo;
	// Annotation required to listen
	// the message from Kafka server
	
	  @KafkaListener(topics = "StudentExample", groupId = "id", containerFactory =
	  "containerFactory")
	  
	  @SendTo() 
	  public Result publish(@Payload Student student) {
	  System.out.println("New Entry: " + student.toString()); 
	  Result res= new Result(); 
	  res.setId(new Random().nextInt());
	  res.setName("ria"); 
	  res.setPercentage("50"); 
	  repo.save(res); 
	  return res; 
	  }
	 
	
	/*
	 * @KafkaListener(topics = "StudentExample", groupId = "id", containerFactory =
	 * "containerFactory")
	 * 
	 * @SendTo() public Student publish(@Payload Student student) {
	 * System.out.println("New Entry: " + student.toString()); student.setId(new
	 * Random().nextInt()); student.setFirstName(student.getFirstName()+"ria");
	 * //res.setPercentage("50"); repo.save(student); return student; }
	 */
}
