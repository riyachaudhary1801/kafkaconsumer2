package com.example.KafkaConsumer;

import java.util.HashMap;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.ErrorHandlingDeserializer;
import org.springframework.kafka.support.serializer.JsonDeserializer;
import org.springframework.kafka.support.serializer.JsonSerializer;



@EnableKafka
@Configuration
public class Config {

	// Function to establish a connection
	// between Spring application
	// and Kafka server

	@Bean
	public ProducerFactory<String, Student> producerFactory() {
		// Create a map of a string
		// and object
		Map<String, Object> config = new HashMap<>();

		config.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");

		config.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);

		config.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
		config.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
		// config.put(JsonSerializer.TYPE_MAPPINGS,"com.example.KafkaProducer.Student");

		return new DefaultKafkaProducerFactory<String, Student>(config);
	}

	@Bean
	public KafkaTemplate<String, Student> kafkaTemplate() {
		return new KafkaTemplate<>(producerFactory());
	}

	@Bean
	public ConsumerFactory<String, Student> studentConsumer() {

		// HashMap to store the configurations
		Map<String, Object> map = new HashMap<>();

		map.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		map.put(ErrorHandlingDeserializer.KEY_DESERIALIZER_CLASS, ErrorHandlingDeserializer.class);
		map.put(ErrorHandlingDeserializer.VALUE_DESERIALIZER_CLASS, ErrorHandlingDeserializer.class);
		map.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		map.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, JsonDeserializer.class);
		map.put(JsonDeserializer.VALUE_DEFAULT_TYPE, "com.example.KafkaConsumer.Student");
		map.put(JsonDeserializer.USE_TYPE_INFO_HEADERS, false);
		map.put(JsonDeserializer.TRUSTED_PACKAGES, "*");
		ErrorHandlingDeserializer<Student> errorHandlingDeserializer = new ErrorHandlingDeserializer<>(
				new JsonDeserializer<>(Student.class));

		return new DefaultKafkaConsumerFactory<String, Student>(map, new StringDeserializer(),
				errorHandlingDeserializer);
	}

	@Bean
	ConcurrentKafkaListenerContainerFactory<String, Student> containerFactory() {
		ConcurrentKafkaListenerContainerFactory<String, Student> factory = new ConcurrentKafkaListenerContainerFactory<>();
		factory.setConsumerFactory(studentConsumer());
		factory.setConcurrency(3);
		factory.getContainerProperties().setPollTimeout(3000);
		// Set kafkaTemplate to support sendTo
		factory.setReplyTemplate(kafkaTemplate());
		return factory;
	}
}
