package com.example.KafkaConsumer;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Student {
	 // Data members of the
    // student class
	@Id
    int id;
    String firstName;
    String lastName;
   
 // Constructor of the student
    // class
    
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	 @Override
	    public String toString() {
	        return this.id + " + " + this.firstName +" "+ this.lastName;
	    }
  

}
